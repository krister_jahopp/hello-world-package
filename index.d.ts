declare module "@krister_jahopp/hello-world-package" {
    function sayHello(string): string;
    export = sayHello;
}