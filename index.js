/**
 * Function that returns a greeting including the name
 * 
 * @param {string} name
 * @returns {string} A greeting including the name
 */
export function sayHello(name) {
    return `Hello ${name}`;
}